// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add("login", () => {
  cy.visit("http://127.0.0.1:5500/frontend/www/login.html");
  cy.get("#form-login").within((el) => {
    cy.fixture("auth.json").then((json) => {
      cy.get('input[name="username"]').type(json.user.name);
      cy.get('input[name="password"]').type(json.user.password);
    });
  });
  cy.get("#btn-login").click();
  expect(cy.getCookie("refresh")).to.exist;
  expect(cy.getCookie("Authorization")).to.exist;
  expect(cy.getCookie("access")).to.exist;
});

Cypress.Commands.add("loginSecondUser", () => {
  cy.visit("http://127.0.0.1:5500/frontend/www/login.html");
  cy.get("#form-login").within((el) => {
    cy.fixture("auth.json").then((json) => {
      cy.get('input[name="username"]').type(user2);
      cy.get('input[name="password"]').type(user2);
    });
  });
  cy.get("#btn-login").click();

  //expect(cy.getCookie("access")).to.exist;     cy.loginSecondUser();
});
