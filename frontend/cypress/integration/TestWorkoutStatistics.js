describe("Exercise boundary test", () => {
  beforeEach(() => {
    cy.login().then(() => {
      cy.visit("http://127.0.0.1:5500/frontend/www/exercises.html", {
        auth: { username: "admin", password: "Password" },
      });
      Cypress.Cookies.preserveOnce(document.cookie);
      Cypress.Cookies.preserveOnce("access");
    });
  });

  beforeEach(() => {
    cy.visit("http://127.0.0.1:5500/frontend/www/workouts.html");
  });

  it("test workout statistics work", () => {
    let request;
    let request2;

    const token = document.cookie
      .split("; ")
      .find((row) => row.startsWith("access="))
      .split("=")[1];
    cy.request({
      method: "GET",
      headers: { authorization: "Bearer " + token },
      url: "http://localhost:8000/api/statistics/",
    })
      .then(({ response }) => {
        request = response.body.length;
        cy.get("#div-content").first().wait(1000).click();
        cy.get("#btn-complete-workout").click();
      })
      .then(() => {
        cy.request({
          method: "GET",
          headers: { authorization: "Bearer " + token },
          url: "http://localhost:8000/api/statistics/",
        })
          .then((response) => {
            request2 = response.body.length;
          })
          .then(() => {
            expect(request + 1).to.equal(request2);
          });
      });
  });
});
