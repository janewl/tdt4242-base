/// <reference types="cypress" />

describe("Exercise boundary test", () => {
  beforeEach(() => {
    cy.login().then(() => {
      cy.visit("http://127.0.0.1:5500/frontend/www/exercises.html", {
        //when running locally with live server
        auth: { username: "admin", password: "Password" },
      });
      Cypress.Cookies.preserveOnce(
        "access",
        "refresh",
        "Authorization",
        "sessionid",
        "csrftoken"
      );
    });
  });

  it("Create new exercise with nominal input", () => {
    cy.get("#btn-create-exercise").click();
    cy.get("#inputName").type("Best exercise ever!");
    cy.get("#inputDescription").type("so fun");
    cy.get("#inputUnit").type("3");
    cy.get("#inputDuration").type("3");
    cy.get("#inputCalories").type("3");
    cy.get('select[name="muscleGroup"]').select("Shoulders");
    cy.get("#btn-ok-exercise").click();
    cy.url().should("include", "/exercises.html");
  });

  it("Excercise with min inputs", () => {
    cy.get("#btn-create-exercise").click();
    cy.get("#inputName").type("Best exercise ever!");
    cy.get("#inputDescription").type("so fun");
    cy.get("#inputUnit").type("a");
    cy.get("#inputDuration").type("0");
    cy.get("#inputCalories").type("0");
    cy.get('select[name="muscleGroup"]').select("Shoulders");
    cy.get("#btn-ok-exercise").click();
    cy.url().should("include", "/exercises.html");
  });

  it("Excercise with just above min inputs", () => {
    cy.get("#btn-create-exercise").click();
    cy.get("#inputName").type("Best exercise ever!");
    cy.get("#inputDescription").type("so fun");
    cy.get("#inputUnit").type("aa");
    cy.get("#inputDuration").type("1");
    cy.get("#inputCalories").type("1");
    cy.get('select[name="muscleGroup"]').select("Shoulders");
    cy.get("#btn-ok-exercise").click();
    cy.url().should("include", "/exercises.html");
  });

  it("Excercise with max inputs", () => {
    cy.get("#btn-create-exercise").click();
    cy.get("#inputName").type("Best exercise ever!");
    cy.get("#inputDescription").type("so fun");
    cy.get("#inputUnit").type("a".repeat(49));
    cy.get("#inputDuration").type("1000000000000000000");
    cy.get("#inputCalories").type("1000000000000000000");
    cy.get('select[name="muscleGroup"]').select("Shoulders");
    cy.get("#btn-ok-exercise").click();
    cy.url().should("include", "/exercises.html");
  });
  it("Excercise with just below max inputs", () => {
    cy.get("#btn-create-exercise").click();
    cy.get("#inputName").type("Best exercise ever!");
    cy.get("#inputDescription").type("so fun");
    cy.get("#inputUnit").type("a".repeat(48));
    cy.get("#inputDuration").type("9999999999999998");
    cy.get("#inputCalories").type("9999999999999998");
    cy.get('select[name="muscleGroup"]').select("Shoulders");
    cy.get("#btn-ok-exercise").click();
    cy.url().should("include", "/exercises.html");
  });
});
