describe("Rating integration test", () => {
  before(() => {
    cy.login().then(() => {
      cy.visit("http://127.0.0.1:5500/frontend/www/workouts.html", {
        auth: { username: "admin", password: "Password" },
      });
      Cypress.Cookies.preserveOnce(
        "access",
        "refresh",
        "Authorization",
        "sessionid",
        "csrftoken"
      );
    });
  });

  it("Test if rating a workout work", () => {
    cy.get("#third").first().click();
    cy.get("#first").should("have.css", "color", "rgb(255, 165, 0)");
    cy.get("#second").should("have.css", "color", "rgb(255, 165, 0)");
    cy.get("#third").should("have.css", "color", "rgb(255, 165, 0)");
  });

  it("Test if average rating is calculated correctly", () => {
    cy.get("#btn-logout").click().wait(1000);
    cy.loginSecondUser().then(() => {
      cy.get("#second").first().wait(1000).click();
      cy.get("#first").should("have.css", "color", "rgb(255, 165, 0)");
      cy.get("#second").should("have.css", "color", "rgb(255, 165, 0)");
      cy.get("#randomId").first().should("have.text", "2.5");
    });
  });
});
