/// <reference types="cypress" />

describe("Register user boundary test", () => {
  it("Register user with just less than max length", () => {
    cy.visit("http://127.0.0.1:5500/frontend/www/register.html");
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("a".repeat(136) + Date.now());
      cy.get('input[name="password"]').type("123");
      cy.get('input[name="password1"]').type("123");
      cy.get('input[name="phone_number"]').type("x".repeat(49));
      cy.get('input[name="country"]').type("x".repeat(49));
      cy.get('input[name="city"]').type("x".repeat(49));
      cy.get('input[name="street_address"]').type("x".repeat(49));
    });
    cy.get("#btn-create-account").click();
    cy.url().should("include", "/workouts.html");

    cy.get("#btn-logout").click();
  });

  it("Register user with max length", () => {
    cy.visit("http://127.0.0.1:5500/frontend/www/register.html");
    cy.get("#form-register-user").within(($form) => {
      cy.get('input[name="username"]').type("a".repeat(137) + Date.now());
      cy.get('input[name="password"]').type("123"); //has no max length
      cy.get('input[name="password1"]').type("123"); //has no max length
      cy.get('input[name="phone_number"]').type("x".repeat(50));
      cy.get('input[name="country"]').type("x".repeat(50));
      cy.get('input[name="city"]').type("x".repeat(50));
      cy.get('input[name="street_address"]').type("x".repeat(50));
    });
    cy.get("#btn-create-account").click();
    cy.get("#btn-create-account")
      .click()
      .then(() => {
        cy.url().should("include", "/workouts.html");
      });
    cy.get("#btn-logout").click();
  });

  it("Register user with min length", () => {
    cy.visit("http://127.0.0.1:5500/frontend/www/register.html");
    cy.get("#form-register-user").within(($form) => {
      // generate random username to avoid errors!
      let username = Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 1);
      cy.get('input[name="username"]').type(username);
      cy.get('input[name="password"]').type("x");
      cy.get('input[name="password1"]').type("x");
    });
    cy.get("#btn-create-account")
      .click()
      .then(() => {
        cy.url().should("include", "/workouts.html");
      });
    cy.get("#btn-logout").click();
  });

  it("Register user with just above min length", () => {
    cy.visit("http://127.0.0.1:5500/frontend/www/register.html");
    cy.get("#form-register-user").within(($form) => {
      // generate random username to avoid errors!
      let username = Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 2);
      cy.get('input[name="username"]').type(username);
      cy.get('input[name="email"]').type("x@x.xo");
      cy.get('input[name="password"]').type("xx");
      cy.get('input[name="password1"]').type("xx");
      cy.get('input[name="country"]').type("x");
      cy.get('input[name="city"]').type("x");
      cy.get('input[name="street_address"]').type("x");
    });
    cy.get("#btn-create-account")
      .click()
      .then(() => {
        cy.url().should("include", "/workouts.html");
      });
  });
});
