let statistics
let graph
let myChart
let weeklyWorkoutCount
let monthlyWorkoutCount
let currentSorting = "week"

const monthLabels = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'july', 
    'august',
    'september',
    'october',
    'november',
    'december',
  ];

  const weekdayLabels = [
      'Monday', 
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
  ]

window.addEventListener("DOMContentLoaded", async () => {
  statistics = await getUserWorkoutStatistics();
  weeklyWorkoutCount = countWorkoutsByCurrentWeek();
  monthlyWorkoutCount = countWorkoutsByCurrentMonth();
  createStatisticsChart(weeklyWorkoutCount);
})

var sortingButtons = document.querySelectorAll('input[name = "sorting"]');
sortingButtons.forEach((radio) => {
  radio.addEventListener("change", () => {
    currentSorting = radio.id
    if(currentSorting == "week"){
      updateGraph(myChart, weekdayLabels, weeklyWorkoutCount);
    }else {
      updateGraph(myChart, monthLabels, monthlyWorkoutCount);
    }
  })
})

function updateGraph(chart, labels, data) {
  chart.data.datasets.pop();
  chart.data.datasets.push({
    label: 'How many workouts you have completed',
    data: data,
    backgroundColor: 'rgb(255, 99, 132)',
    borderColor: 'rgb(255, 99, 132)',
    borderWidth: 1
  });
  chart.data.labels = [];
  chart.data.labels = labels;
  chart.update();
}


//Retrieves the completed workouts of a user
async function getUserWorkoutStatistics(){
  let response = await sendRequest('GET', `${HOST}/api/statistics/`);
  if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
  } else {
      let data = await response.json();
      return data.results;
  }
}

//create statistics chart
  function createStatisticsChart(data) {
    graph = document.getElementById('myChart');
    myChart = new Chart(graph, {
      type: 'line',
      data: {
          labels: weekdayLabels,
          datasets: [{
              label: 'How many workouts you have completed',
              data: data,
              backgroundColor: 'rgb(255, 99, 132)',
              borderColor: 'rgb(255, 99, 132)',
              borderWidth: 1
          }]
      },
      options: {
      }
  });
    return myChart
  }
  
  //count workouts (by week)
  function countWorkoutsByCurrentWeek() {
    let workoutCount = [0, 0, 0, 0, 0, 0, 0];
    const currentWeek = getWeek(new Date())
    statistics.forEach((entity) => 
    {
      const workoutDate = new Date(entity.date)
      const workoutCompletedWeek = getWeek(workoutDate)
      if(workoutCompletedWeek === currentWeek){
        const dayOfWeek = workoutDate.getDay();
        workoutCount[dayOfWeek-1] += 1;
      }
    })
    return workoutCount;
  }

  function countWorkoutsByCurrentMonth() {
    let workoutCount = new Array(12).fill(0);
    const currentYear = new Date().getFullYear()
    statistics.forEach((entity) => {
      const workoutDate = new Date(entity.date);
      const yearCompleted = workoutDate.getFullYear();
      if(yearCompleted == currentYear){
        const monthCompleted = workoutDate.getMonth();
        workoutCount[monthCompleted] += 1 
      }
    })
    return workoutCount
  }
  
// Source: https://weeknumber.com/how-to/javascript
// Returns the ISO week of the date.
function getWeek(date) {
  var newdate = new Date(date);
  newdate.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  newdate.setDate(newdate.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(newdate.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((newdate.getTime() - week1.getTime()) / 86400000
                        - 3 + (week1.getDay() + 6) % 7) / 7);
}
