let workout_ids = null;
let spesificWorkout;

async function fetchWorkouts(ordering) {
  let response = await sendRequest(
    "GET",
    `${HOST}/api/workouts/?ordering=${ordering}`
  );

  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  } else {
    let data = await response.json();

    let workouts = data.results;
    let container = document.getElementById("div-content");
    workouts.forEach((workout) => {
      let templateWorkout = document.querySelector("#template-workout");
      let cloneWorkout = templateWorkout.content.cloneNode(true);

      let aWorkout = cloneWorkout.querySelector("a");
      aWorkout.href = `workout.html?id=${workout.id}`;

      let bWorkout = cloneWorkout.querySelector("#StarRatingForm");
      bWorkout.onclick = () => {
        workout_ids = `http://localhost:8000/api/workouts/${workout.id}`;
        spesificWorkout = workout;
      };

      let h5 = aWorkout.querySelector("h5");
      h5.textContent = workout.name;

      let localDate = new Date(workout.date);

      let table = aWorkout.querySelector("table");
      let rows = table.querySelectorAll("tr");
      rows[0].querySelectorAll("td")[1].textContent =
        localDate.toLocaleDateString(); // Date
      rows[1].querySelectorAll("td")[1].textContent =
        localDate.toLocaleTimeString(); // Time
      rows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
      rows[3].querySelectorAll("td")[1].textContent =
        workout.exercise_instances.length; // Exercises

      container.appendChild(aWorkout);
    });
    return workouts;
  }
}

function createWorkout() {
  window.location.replace("workout.html");
}

async function fetchRatings() {
  let response = await sendRequest("GET", `${HOST}/api/rate/`);
  if (response.ok) {
    let data1 = await response.json();
    let data = data1.results;
    console.log(data);
    return data;
  }
}

const one = null;
const two = null;
const three = null;
const four = null;
const five = null;
let RateArr = [];
let form = null;

window.addEventListener("DOMContentLoaded", async () => {
  let createButton = document.querySelector("#btn-create-workout");
  createButton.addEventListener("click", createWorkout);
  let ordering = "-date";

  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has("ordering")) {
    let aSort = null;
    ordering = urlParams.get("ordering");
    if (ordering == "name" || ordering == "owner" || ordering == "date") {
      let aSort = document.querySelector(`a[href="?ordering=${ordering}"`);
      aSort.href = `?ordering=-${ordering}`;
    }
  }

  let currentSort = document.querySelector("#current-sort");
  currentSort.innerHTML =
    (ordering.startsWith("-") ? "Descending" : "Ascending") +
    " " +
    ordering.replace("-", "");

  let currentUser = await getCurrentUser();
  // grab username
  if (ordering.includes("owner")) {
    ordering += "__username";
  }
  let workouts = await fetchWorkouts(ordering);

  let rating = await fetchRatings();
  let data = [];
  avgRating = {};
  nrOfRating = {};

  workouts.forEach((element) => {
    avgRating[element.url] = 0;
  });

  rating.forEach((element) => {
    avgRating[element.workout] = avgRating[element.workout] + element.rating;
  });

  rating.forEach((element) => {
    nrOfRating[element.workout] = (nrOfRating[element.workout] || 0) + 1;
  });

  for (let element in avgRating) {
    if (nrOfRating[element]) {
      avgRating[element] = avgRating[element] / nrOfRating[element];
    }
  }

  rating.forEach((el) => {
    if (el.owner == currentUser.username) data.push(el);
  });

  rating = data;

  let tabEls = document.querySelectorAll('a[data-bs-toggle="list"]');
  for (let i = 0; i < tabEls.length; i++) {
    let tabEl = tabEls[i];
    tabEl.addEventListener("show.bs.tab", function (event) {
      let workoutAnchors = document.querySelectorAll(".workout");
      for (let j = 0; j < workouts.length; j++) {
        // I'm assuming that the order of workout objects matches
        // the other of the workout anchor elements. They should, given
        // that I just created them.
        let workout = workouts[j];
        let workoutAnchor = workoutAnchors[j];

        switch (event.currentTarget.id) {
          case "list-my-workouts-list":
            if (workout.owner == currentUser.url) {
              workoutAnchor.classList.remove("hide");
            } else {
              workoutAnchor.classList.add("hide");
            }
            break;
          case "list-athlete-workouts-list":
            if (
              currentUser.athletes &&
              currentUser.athletes.includes(workout.owner)
            ) {
              workoutAnchor.classList.remove("hide");
            } else {
              workoutAnchor.classList.add("hide");
            }
            break;
          case "list-public-workouts-list":
            if (workout.visibility == "PU") {
              workoutAnchor.classList.remove("hide");
            } else {
              workoutAnchor.classList.add("hide");
            }
            break;
          default:
            workoutAnchor.classList.remove("hide");
            break;
        }
      }
    });
  }
  const one = document.querySelectorAll("#first");
  const two = document.querySelectorAll("#second");
  const three = document.querySelectorAll("#third");
  const four = document.querySelectorAll("#fourth");
  const five = document.querySelectorAll("#fifth");
  form = document.querySelectorAll(".rate-form");
  const u = [];
  workouts.forEach((i) => u.push(i.url));
  form.forEach((formEl, index) => {
    const nrOne = one[index];
    const nrTwo = two[index];
    const nrThree = three[index];
    const nrFour = four[index];
    const nrFive = five[index];
    const array = [nrOne, nrTwo, nrThree, nrFour, nrFive];
    const arr = { id: u[index], arr: array };
    RateArr.push(arr);
    formEl.addEventListener(
      "click",
      starRate(array, formEl, currentUser.username)
    );
  });

  avgRatingDisplys = [];

  allAvgRatingDisplays = document.querySelectorAll("#randomId");

  allAvgRatingDisplays.forEach((el, index) => {
    avgRatingDisplys.push({
      workout: Object.keys(avgRating)[index],
      diplay: el,
    });
  });

  Object.keys(avgRating).forEach((el, index) => {
    if (el === avgRatingDisplys[index].workout) {
      if (avgRating[el].toFixed(1) === "0.0") {
        avgRatingDisplys[index].diplay.innerHTML = "No ratigns yet";
      } else {
        avgRatingDisplys[index].diplay.innerHTML =
          "Average rating: " + avgRating[el].toFixed(1) + " stars";
      }
    }
  });

  RateArr.forEach((u) => {
    rating.forEach((a) => {
      if (a.workout === u.id) {
        handleStarSelect(a.rating, u.arr);
      }
    });
  });
});

const confirmBox = document.getElementById("confirm-box");
const csrf = document.getElementsByName("csrfmiddlewaretoken");

const handleStarSelect = (size, RateArr, formEl) => {
  const children = RateArr;
  for (let i = 0; i < children.length; i++) {
    if (i < size) {
      children[i].classList.add("checked");
    } else {
      children[i].classList.remove("checked");
    }
  }
};

const handleSelect = (selection, RateArr, formEl) => {
  switch (selection) {
    case "first": {
      handleStarSelect(1, RateArr, formEl);
      return;
    }
    case "second": {
      handleStarSelect(2, RateArr, formEl);
      return;
    }
    case "third": {
      handleStarSelect(3, RateArr, formEl);
      return;
    }
    case "fourth": {
      handleStarSelect(4, RateArr, formEl);
      return;
    }
    case "fifth": {
      handleStarSelect(5, RateArr, formEl);
      return;
    }
    default: {
      handleStarSelect(0, RateArr, formEl);
    }
  }
};

const getNumericValue = (stringValue) => {
  let numericValue;
  if (stringValue === "first") {
    numericValue = 1;
  } else if (stringValue === "second") {
    numericValue = 2;
  } else if (stringValue === "third") {
    numericValue = 3;
  } else if (stringValue === "fourth") {
    numericValue = 4;
  } else if (stringValue === "fifth") {
    numericValue = 5;
  } else {
    numericValue = 0;
  }
  return numericValue;
};

async function nowStarRate(val_num, ids, currentUser) {
  console.log(spesificWorkout);
  console.log(ids);

  let body = {
    rating: val_num,
    workout: spesificWorkout,
    id: ids,
    owner: currentUser,
  };
  let response = await sendRequest("POST", `${HOST}/api/rate/`, body);
  if (response.ok) {
    console.log("ok");
  } else {
    console.log("!ok");
  }
}

const starRate = (RateArr, formEl, currentUser) => {
  RateArr.forEach((item) =>
    item.addEventListener("click", (event) => {
      handleSelect(event.target.id, RateArr);
    })
  );

  let val = 0;
  RateArr.forEach((item) =>
    item.addEventListener("click", (event) => {
      val = event.target.id;

      formEl.addEventListener("submit", async (e) => {
        e.preventDefault();
        const val_nums = getNumericValue(val);
        val_num = parseInt(val_nums);
        ids = parseInt(workout_ids.slice(-2).replace(/\D/g, ""));
        ids2 = workout_ids.slice(-2).replace(/\D/g, "");
        let workoutData = await nowStarRate(val_num, ids, currentUser);
      });
    })
  );
};
