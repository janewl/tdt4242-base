from rest_framework import permissions
from users.models import User
from django.test import TestCase
import datetime
import pytz
from workouts.models import Workout
from django.test import TestCase
from workouts.permissions import IsOwner, IsPublic, IsCoachOfWorkoutAndVisibleToCoach, IsOwnerOfWorkout, IsCoachAndVisibleToCoach , IsWorkoutPublic, IsReadOnly

class TestDataWorkout:
    def __init__(self):
        owner = User.objects.create()
        self.workout = Workout.objects.create(**({
            "name": "Abolutely just fun",
            "date": datetime.datetime(2022, 4, 19, 10, 10, 10, 10, pytz.timezone('UTC')), 
            "notes": "fun fun fun",
            "visibility": "PU",
            "owner": owner,
        }))
        self.workout.owner.coach = User()

class Request:
  
    def __init__(self):
        self.method = ""
        self.user = None
        self.data = ""

class TestIfOwner(TestCase):
    
    def setUp(self):
        self.owner = IsOwner()
        self.request = Request()
        self.request.user = User()
        self.workout = TestDataWorkout()

    def test_permissionTest(self):
        self.assertIs(self.owner.has_object_permission(
            self.request, "", self.workout.workout), False)
        self.request.user = self.workout.workout.owner
        self.assertIs(self.owner.has_object_permission(
            self.request, "", self.workout.workout), True)

class TestIfOwnerOfWorkout(TestCase):
    def setUp(self):
        self.owner = IsOwnerOfWorkout()
        self.workout = TestDataWorkout()
        self.request = Request()

    def test_checkUserPermission(self):
        request = Request()
        request.method = "POST"
        request.user = self.workout.workout.owner
        request.data = {
            "workout": "http://localhost:8000/api/workouts/1/"
        }
        self.assertIs(self.owner.has_permission(
            request, ""
        ), True)
 
    def test_checkWorkoutPermission(self):
        request = Request()
        request.method = "POST"
        request.data = {}
        self.assertIs(self.owner.has_permission(request, ""), False)

    def test_checkPermissions(self):
        request = Request()
        request.method = "GET"
        self.assertIs(self.owner.has_permission(request, "" ), True)


    def test_checkOwnership(self):
        self.assertIs(self.owner.has_object_permission(
            self.request, "", self.workout), False)
        self.request.user = self.workout.workout.owner
        self.assertIs(self.owner.has_object_permission(
            self.request, "", self.workout), True)


class TestIfCoachAndHasCoachPermissions(TestCase):
    def setUp(self):
        self.request = Request()
        self.request.user = User()
        self.workout = TestDataWorkout()
        self.is_coach_and_visible_to_coach = IsCoachAndVisibleToCoach()


    def test_checkPermissions(self):
        self.assertIs(self.is_coach_and_visible_to_coach.has_object_permission(
            self.request, "", self.workout.workout), False)

        self.workout.workout.owner.coach = self.request.user
        self.assertIs(self.is_coach_and_visible_to_coach.has_object_permission(
            self.request, "", self.workout.workout ), True)


class TestCoachCanSeeWorkout(TestCase):

    def setUp(self):
        self.request = Request()
        self.request.user = User()
        self.workout = TestDataWorkout()
        self.visible = IsCoachOfWorkoutAndVisibleToCoach()


    def test_checkCoachpermission(self):
        self.assertIs(self.visible.has_object_permission(
            self.request,"", self.workout), False)
        self.workout.workout.owner.coach = self.request.user
        self.assertIs(self.visible.has_object_permission(
            self.request,"", self.workout), True)


class TestIfPublic(TestCase):

    def setUp(self):
        self.workout = TestDataWorkout()
        self.public = IsPublic()

    def test_checkWorkoutPermission(self):
        self.assertIs(self.public.has_object_permission(
            "","", self.workout.workout), True)
        self.workout.workout.visibility = "CO"
        self.assertIs(self.public.has_object_permission(
            "","", self.workout.workout), False)


class TestPublicWorkout(TestCase):
    def setUp(self):
        self.public = IsWorkoutPublic()
        self.workout = TestDataWorkout()

    def test_checkPermission(self):
        self.assertIs(self.public.has_object_permission(
            "", "", self.workout), True)

        self.workout.workout.visibility = "N"
        self.assertIs(self.public.has_object_permission(
            "","", self.workout), False)


class TestReadOnly(TestCase):
    def setUp(self):
        self.request = Request()
        self.is_read_only = IsReadOnly()
        self.request.method = permissions.SAFE_METHODS[1]

    def test_CheckPermission(self):
        self.assertIs(self.is_read_only.has_object_permission(self.request, None, None), True)
        self.request.method = None
        self.assertIs(self.is_read_only.has_object_permission(self.request, None, None), False)
