from django.test import TestCase, Client

checks = {
    'valid':
        {
            'username': 'Bruker1',
            'email': 'blablabla@ntnu.no',
            'password': 'password',
            'password1': 'password',
            'phone_number': '1234567890',
            'country': 'Norge',
            'city': 'Trondheim',
            'street_address': 'Høyskoleringen',
        },
    'invalid': {
        'username': 'dette er en bruker',
        'email': 'a',
        'password': None,
        'password1': None,
        'phone_number': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'country': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'city': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'street_address': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    },
    'blank': {
        'username': '',
        'email': '',
        'password': '',
        'password1': '',
        'phone_number': '',
        'country': '',
        'city': '',
        'street_address': '',
    }
}

tests = [
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "invalid",
            "country": "valid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "invalid",
            "country": "blank",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "blank",
            "country": "valid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "valid",
            "phone_number": "valid",
            "country": "invalid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "valid",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "invalid",
            "country": "invalid",
            "city": "blank",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "valid",
            "country": "valid",
            "city": "invalid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "blank",
            "city": "valid",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "blank",
            "street_address": "blank"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "invalid",
            "city": "valid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "valid",
            "email": "valid",
            "phone_number": "blank",
            "country": "blank",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "invalid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "blank",
            "country": "invalid",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "invalid",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "invalid",
            "country": "valid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "valid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "blank",
            "city": "invalid",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "valid",
            "country": "blank",
            "city": "valid",
            "street_address": "blank"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "blank",
            "country": "invalid",
            "city": "blank",
            "street_address": "invalid"
        },
        {
            "username": "valid",
            "password": "valid",
            "password1": "valid",
            "email": "blank",
            "phone_number": "invalid",
            "country": "valid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "valid",
            "password": "blank",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "blank",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "invalid",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "blank",
            "email": "valid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "invalid",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "blank",
            "phone_number": "blank",
            "country": "valid",
            "city": "invalid",
            "street_address": "blank"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "invalid",
            "phone_number": "blank",
            "country": "blank",
            "city": "valid",
            "street_address": "invalid"
        },
        {
            "username": "blank",
            "password": "blank",
            "password1": "valid",
            "email": "valid",
            "phone_number": "invalid",
            "country": "blank",
            "city": "blank",
            "street_address": "valid"
        },
        {
            "username": "blank",
            "password": "valid",
            "password1": "blank",
            "email": "blank",
            "phone_number": "valid",
            "country": "invalid",
            "city": "blank",
            "street_address": "blank"
        }
]



class RegistrationTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test(self):
        for tc in tests:
            valid = True
            request = {}
            print(" ")
            for key, value in tc.items():
                request[key] = checks[value][key]
                if  value == 'valid':
                    print("valid")
                if value == 'invalid':
                    print("invalid")
                    valid = False
                elif key == ('username' or 'password' or 'password1') and value == 'blank':
                    print("blank")
                    valid = False

            if (valid == False):
                responseError = 400
            elif (valid == True):
                responseError = 201
                
            re = self.client.post('/api/users/', request)

            print(responseError)
            print(re.status_code)
            self.assertEqual(re.status_code, responseError)
            

