import json
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory
from django.test.client import RequestFactory
from rest_framework import status
from users.serializers import UserSerializer
from users.models import User
from django.urls import path
from django.contrib.auth import get_user_model

# Create your tests here.

class UserSerializerTestCase(APITestCase):
   
    def setUp(self):
        #add user instances to test-database
        self.user = User(username="ola", email="ola@testing.com",phone_number="12345678")
        #user2 = User(username="marcus", password="321", email="marcus@testing.com",phone_number="87654321").save()

    def test_expected_serializer_fields(self):
        expected_fields = [
            "url",
            "id",
            "email",
            "username",
            #"password",
            #"password1",
            "athletes",
            "phone_number",
            "country",
            "city",
            "street_address",
            "coach",
            "workouts",
            "coach_files",
            "athlete_files",
        ]
        #serialize first user in data
        serialized = UserSerializer(User.objects.first(), context={'request': None})
        #check that all expected fields are present in the serialized data
        self.assertCountEqual(serialized.data.keys(), expected_fields)

    def test_serialize_data(self):
        expected_result = {
            'url': '/api/users/1/', 
            'id': 1, 
            'email': 'ola@testing.com', 
            'username': 'ola', 'athletes': [], 
            'phone_number': '12345678', 
            'country': '', 
            'city': '', 
            'street_address': '', 
            'coach': None, 
            'workouts': [], 
            'coach_files': [], 
            'athlete_files': []
        }

        serialized = UserSerializer(User.objects.first(), context={'request': None})
        self.assertEqual(serialized.data, expected_result)
    
    def test_create_new(self):
        test_data = {
        "username":"ola",
        "email":"ola@testing.com",
        "password":"123",
        "phone_number": "12345678",
        "country":"",
        "city":"",
        "street_address":"" 
        }
        print(self.user)
        create_user = UserSerializer.create(UserSerializer(), validated_data=test_data)
        self.assertEqual(create_user, self.user)
  

        
